import logging
import os

import leangle
from chalice import Chalice, CognitoUserPoolAuthorizer, Response, IAMAuthorizer
from marshmallow import Schema, ValidationError, fields, validate
from requests.api import get, head

from chalicelib.api.swagger_api import swagger_api
from chalicelib.utils.audience_utils import (
    get_audience_provider, get_email_from_request, get_error_response,
    get_identity_provider, get_registered_audience_for_product,
    get_success_response, user_authorized, prepare_audience, prepare_identity,
    refactor_audience, register_audience_to_product, save_audience_in_db)
from chalicelib.utils.awsclient import (create_api_mapping_v2,
                                        delete_api_mapping_v2)
from chalicelib.utils.awscognito import in_allowed_groups
from chalicelib.utils.swagger_utls import get_swagger_ui

app = Chalice(app_name='audience-api-{}'.format(os.getenv('STAGE_NAME')))

app.es_user = None
app.es_pwd = None

# Register blueprints
app.experimental_feature_flags.update([
    'BLUEPRINTS',
])
app.register_blueprint(swagger_api)

# Set logger
logger = logging.getLogger()
logger.setLevel(logging.INFO)

# Patches
# Temporary fix for multi-route custom domain mapping issue
# On AWS, chalice does not include deploy, so ignore patching it
try:
    from chalice.awsclient import TypedAWSClient
    TypedAWSClient._create_base_path_mapping = create_api_mapping_v2
    TypedAWSClient.delete_api_mapping = delete_api_mapping_v2
except ImportError:
    pass

authorizer = CognitoUserPoolAuthorizer('MyPool', provider_arns=[os.environ['cognitoPoolArn']])
iam_authorizer = IAMAuthorizer()
# Adding cognito group validation for all authorized routes
@app.middleware('http')
def cognito_validate_groups(event, get_response):
    # if in_allowed_groups(event):
    return get_response(event)

    return Response(status_code=403, 
                    body={'message': 'Forbidden: The user must be in one of the allowed groups.'},
                    headers={'Content-Type': 'application/json'})

# Get SwaggerUI main page 
@app.route("/", methods=["GET"])
def get_doc():
    html = get_swagger_ui(app)
    return Response(body=html, status_code=200,
                    headers={"Content-Type": "text/html"},)

@leangle.add_schema()
class SocialProfileSchema(Schema):
    social_type = fields.Str(required=True, validate=validate.OneOf(['google', 'linkedin', 'facebook']), title="The type of social account used to login. Example - Google, LinkedIn, Facebook")
    social_email = fields.Email(required=True, title="The email address used for social accounts")

@leangle.add_schema()
class ConsentSchema(Schema):
    privacy_policy_accepted = fields.Boolean(required=False, validate=validate.OneOf([True, False]), title="Is Informa privacy policy consent given?")
    site_updates = fields.Boolean(required=False, validate=validate.OneOf([True, False]), title="Is site updates consent given?")
    partner_updates = fields.Boolean(required=False, validate=validate.OneOf([True, False]), title="Is partner updates policy consent given?")

@leangle.add_schema()
class ListItemSchema(Schema):
    id = fields.Str(required=True, title='Id')
    division = fields.Str(required=True, title='Division')
    brand = fields.Str(required=True, title='Brand')

@leangle.add_schema()
class AudienceProfileSchema(Schema):
    id = fields.UUID(required=False, title='Audience profile id')
    business_email = fields.Email(required=True, title='Business email address')
    first_name = fields.Str(required=True, title='First name')
    last_name = fields.Str(required=True, title='Last name')
    company = fields.Str(required=True, title='Organization name')
    country = fields.Str(required=True, title='Country of residence')
    state = fields.Str(required=True, title='State/Provience/Territory of residence')
    business_type = fields.Str(required=True, title='Business type')
    business_types = fields.List(fields.Nested(ListItemSchema), required=False, title='Business types')
    job_function = fields.Str(required=True, title='Job function')
    job_functions = fields.List(fields.Nested(ListItemSchema), required=False, title='Job functions')
    job_title = fields.Str(required=True, title='Job title')
    social_accounts = fields.List(fields.Nested(SocialProfileSchema), required=False, title='Social account details if user has logged-in using social account')
    consent = fields.Nested(ConsentSchema(), required=False, title="Is consent given?")
    is_social_login = fields.Boolean(required=True, title='Is trying to login using social accounts?')
    subscribed_for_newsletter = fields.Boolean(required=False, title='Is user subscribed to newsletter?')

def add(request):
    identity_provider = get_identity_provider(app)
    audience_provider = get_audience_provider(app)
    identity = prepare_identity(request)
    audience_identity_id = identity_provider.add_identity(**identity)
    audience_profile = prepare_audience(audience_identity_id, request)
    audience_provider.add_audience_profile(audience_profile)
    save_audience_in_db(audience_profile, False)
    return get_audience(audience_identity_id)

def update(audience_identity_id, request):
    identity_provider = get_identity_provider(app)
    audience_provider = get_audience_provider(app)
    result = audience_provider.update_audience_profile(audience_identity_id, request)
    if result == None:
        return get_error_response( 'Something went wrong.', 500)
    save_audience_in_db(result, True)
    identity = prepare_identity(result)
    identity_provider.update_identity(audience_identity_id, **identity)
    return get_success_response(result)

@app.route('/profile', methods=['POST'], content_types=['application/json'], authorizer=iam_authorizer, cors=True)
@leangle.describe.parameter(name='body', _in='body', description='Add Audience', schema="AudienceProfileSchema")
def save_audience():
    logger.debug("save_audience - entering")
    request = app.current_request.json_body
    logger.debug('save_audience - request - %s', request)

    # check request body
    if request is None:
        return get_error_response('Request Body Empty. Provide Audience Profile.', 400)

    # validate json
    audience_schema = AudienceProfileSchema()
    try:
        audience_schema.load(request)
    except ValidationError as err:
        logger.debug('save_audience - json data invalid - %s', request)
        return get_error_response( 'JSON Data Is Invalid.', 402)

    identity_provider = get_identity_provider(app)

    # if id is in the payload
    # yes - check if identity with the id exists
    if 'id' in request:
        audience_identity_id = request['id']
        result = identity_provider.search_audience_identity_by_id(audience_identity_id)
        logger.debug('save_audience - update by id - %s', result)
        #   yes -   fetch the profile and update it 
        if result['found']:
            logger.debug('save_audience - found by id - %s', audience_identity_id)
            return update(audience_identity_id, request)
        #   no - return error message
        else:
            return get_error_response('Audience Profile Not Found.', 404)

    #   if it a login via social accounts
    #   yes - check if any record has social email coming in request
    if request['is_social_login']:
        social_accounts = request['social_accounts']
        if (social_accounts == None) and (len(social_accounts) == 0):
            return get_error_response( 'JSON Data Is Invalid.', 402)
            
        result = identity_provider.search_audience_identity_by_social_account(social_accounts)
        logger.debug('save_audience - update by social - %s', result)
        audience_identity_id = result['id'] 
        #  yes -   fetch the profile and update it
        if result['found']:
            logger.debug('save_audience - found by social - %s', audience_identity_id)
            return update(audience_identity_id, request)
        #   no  -   if the record with business_email exists
        else:
            business_email = request['business_email']
            result = identity_provider.search_audience_identity_by_email(business_email)
            logger.debug('save_audience - update by email - %s', result)
            audience_identity_id = result['id']
            #   yes -   fetch the profile and update it
            if result['found']:
                logger.debug('save_audience - found by email - %s', audience_identity_id)
                return update(audience_identity_id, request)
            # no  -   add identity and add profile
            else:
                logger.debug('save_audience - add record by social - %s', audience_identity_id)
                return add(request)
    #   no  -   if the record with business_email exists
    else:
        business_email = request['business_email']
        result = identity_provider.search_audience_identity_by_email(business_email)
        logger.debug('save_audience - update by email - %s', result)
        audience_identity_id = result['id']
        #   yes -   fetch the profile and update it
        if result['found']:
            logger.debug('save_audience - found by email - %s', audience_identity_id)
            return update(audience_identity_id, request)
        # no  -   add identity and add profile
        else:
            logger.debug('save_audience - add record by email - %s', audience_identity_id)
            return add(request)

@app.route('/profile/{id}', methods=['GET'], authorizer=iam_authorizer, cors=True)
def get_audience(id):
    logger.debug("get_audience - entering")
    logger.debug('get_audience - audience profile id - %s', id)
    provider = get_audience_provider(app)
    result = provider.get_audience_profile_by_id(id)

    # check if user email is present in auth token request header
    request_email = get_email_from_request(app)
    if request_email == None:
        return get_error_response('User is unauthorized to request the resource.', 401)
                        
    if result['found']:
        audience_profile = result['data'][0]['_source']
        if not user_authorized(request_email, audience_profile):
            return get_error_response('User is unauthorized to request the resource.', 401)
        logger.debug('get_audience - result - %s', result)
        return get_success_response(audience_profile)
    else:
        return get_error_response('Audience Profile Not Found.', 404)

        ######sameer ingo integration

@app.route('/partnerRegistration', methods=['POST'], content_types=['application/json'], authorizer=iam_authorizer, cors=True)
@leangle.describe.parameter(name='body', _in='body', description='Register user from ingo ', schema="AudienceRegistrationSchema")
def partner():
        
        request = app.current_request.json_body
        if request is None:
            return get_error_response('Request Body Empty. Provide User  Profile.', 400)
        else:  
                request['iris_id']="2139729834712809379210837"
                return get_success_response(request)
        

        ####sameer ingoo
@app.route('/profile', methods=['GET'], authorizer=iam_authorizer, cors=True)
def get_audience_by_email():
    logger.debug("get_audience_by_email - entering")
    logger.debug('get_audience_by_email - audience profile id - %s', id)

    # check if user email is present in auth token request header
    request_email = get_email_from_request(app)
    if request_email == None:
        return get_error_response('User is unauthorized to request the resource.', 401)

    business_email = None
    if app.current_request.query_params:
        business_email = app.current_request.query_params.get('business_email')
    
    if(business_email == None):
        return get_error_response( 'Invalid Request.', 402)

    identity_provider = get_identity_provider(app)
    audience_provider = get_audience_provider(app)

    identity_result = identity_provider.search_audience_identity_by_email(business_email)

    logger.debug('get_audience_by_email - found identity by email - %s', identity_result)
    audience_identity_id = identity_result['id']

    if identity_result['found']:
        logger.debug('get_audience_by_email - get audience by email - %s', audience_identity_id)
        audience_result = audience_provider.get_audience_profile_by_id(audience_identity_id)
        logger.debug('get_audience_by_email - result - %s', audience_result)                        
        if audience_result['found']:
            audience_profile = audience_result['data'][0]['_source']
            if not user_authorized(request_email, audience_profile):
                return get_error_response('User is unauthorized to request the resource.', 401)
                
            return get_success_response(audience_profile)
        else:
            return get_error_response('Audience Profile Not Found.', 404)
    else:
        return get_error_response('Audience Profile Not Found.', 404)

@leangle.add_schema()
class AudienceRegistrationSchema(Schema):
    iris_id = fields.UUID(required=True, title='Audience profile id')
    product_id = fields.UUID(required=True, title='App config id')
    product_name = fields.Str(required=True, title='Product name')
    product_type = fields.Str(required=True, title='Product type')
    product_sub_type = fields.Str(required=False, title='Product sub type')
    user_enroll_date = fields.Str(required=True, title='Audience enrollment date')
    questions = fields.Dict(required=False, title='custom questions')

@app.route('/registration', methods=['POST'], content_types=['application/json'], authorizer=authorizer, cors=True)
@leangle.describe.parameter(name='body', _in='body', description='Register user to a product', schema="AudienceRegistrationSchema")
def register_audience():
    request = app.current_request.json_body

    # check request body
    if request is None:
        return get_error_response('Request Body Empty. Provide A Valid Payload.', 400)

    # validate json
    audience_registration_schema = AudienceRegistrationSchema()
    try:
        audience_registration_schema.load(request)
    except ValidationError as err:
        logger.debug('user_registration_to_product - json data invalid - request, error - %s %s', request, err)
        return get_error_response( 'JSON Data Is Invalid.', 402)
    
    # check if audience id is present for user
    audience_profile_id = request["iris_id"]
    provider = get_audience_provider(app)
    result = provider.get_audience_profile_by_id(audience_profile_id)
    logger.debug('register_audience - get_audience - result - %s', result)                        
    if result['found']:
        audience_profile = refactor_audience(result['data'][0]['_source'])
    else:
        return get_error_response('Audience Profile Not Found.', 404)

    return register_audience_to_product(audience_profile, request)

@app.route('/registration/{audience_profile_id}/{product_id}', methods=['GET'], authorizer=authorizer, cors=True)
def get_registered_audience(audience_profile_id, product_id):
    return get_registered_audience_for_product(audience_profile_id, product_id)

import json
import os
import uuid

from elasticsearch import Elasticsearch, helpers
from elasticsearch.exceptions import NotFoundError


class IdentityProvider:

    def __init__(self, index, user, pwd) -> None:
        self._index = index
        self._user = user
        self._pwd = pwd
        self._host = os.environ['HOST']
        self._port = int(os.environ["PORT"])
        self._use_ssl = bool(os.environ["USE_SSL"])
        self._elasticsearch = Elasticsearch(
            host=self._host, http_auth=(self._user, self._pwd), 
            port=self._port, use_ssl=self._use_ssl, verify_certs=False)

    @property
    def index_name(self):
        return self._index

    @property
    def es(self):
        return self._elasticsearch

    @property
    def threshold(self):
        return float(os.environ['MATCHING_THRESHOLD'])

    # used
    def add_identity(self, **kwargs):
        id = str(uuid.uuid4())
        data = [{
            '_index': self.index_name,
            '_id': id,
            '_type': '@audience',            
        }]
        data[0].update(kwargs)        
        helpers.bulk(self.es, data)
        return id

    def update_identity(self, id, **kwargs):
        data = [{
            '_index': self.index_name,
            '_id': id,
            '_type': '@audience',            
        }]
        data[0].update(kwargs)        
        helpers.bulk(self.es, data)

    # to be used
    def search_identity(self, email, fuzzy, **kwargs):
        _additional_attributes = ['first_name', 'last_name']
        _query = {
            "query": {     
                "bool": {
                    "must": [
                        { "match": { 'email.keyword': email}},
                        { "match": { '_type': '@audience'}}
                    ]
                }
            }
        }      
               
        if fuzzy:
            for attribute in _additional_attributes:
                if attribute in kwargs:
                    _query['query']['bool']['must'].append({ "match": { attribute: {
                        'query': kwargs[attribute],
                        'fuzziness': 'AUTO'}}})            
        else:
            for attribute in _additional_attributes:
                if attribute in kwargs:
                    _query['query']['bool']['must'].append({ "match": { attribute + '.keyword': kwargs[attribute]}})

        return self.es.search(body=json.dumps(_query), index=self.index_name)

    # used
    def search_audience_identity_by_id(self, id):
        found = False
        result = None
        try:
            result = self.es.get(index=self.index_name, id=id)
            found = (result['found'])
            if found:
                return {
                    'found': found,
                    'id': result['_id']
                }
            else:
                return {
                    'found': found,
                    'id': None
                }
        except NotFoundError as e:
            return {
                    'found': False,
                    'id': None
                }

    # used
    def search_audience_identity_by_social_account(self, social_accounts):
        found = False
        result = None

        social_type = social_accounts[0]['social_type']
        social_email = social_accounts[0]['social_email']
        query = {
            "query": {     
                "bool": {
                    "must": [
                        { "match": { 'social_accounts.social_type.keyword': social_type}},
                        { "match": { 'social_accounts.social_email.keyword': social_email}},
                        { "match": { '_type': '@audience'}}
                    ]
                }
            }
        }
        try:
            result = self.es.search(body=json.dumps(query), index=self.index_name)
            found = (result['hits']['total']['value'] > 0)
            if found:
                return {
                    'found': found,
                    'id': result['hits']['hits'][0]['_id']
                }
            else:
                return {
                    'found': found,
                    'id': None
                }
        except NotFoundError as e:
            found = False
            return {
                    'found': found,
                    'id': None
                }

    # used
    def search_audience_identity_by_name(self, **kwargs):
        additional_attributes = ['first_name', 'last_name']
        query = {
            "query": {     
                "bool": {
                    "must": [
                        { "match": { '_type': '@audience'}}
                    ]
                }
            }
        }      

        for attribute in additional_attributes:
            if attribute in kwargs:
                query['query']['bool']['must'].append({ "match": { attribute: { 'query': kwargs[attribute], 'fuzziness': 'AUTO'}}})            
        try:
            result = self.es.search(body=json.dumps(query), index=self.index_name)
            found = ((result['hits']['total']['value'] > 0) and (result['hits']['max_score'] >= self.threshold))
            if found:
                return {
                    'found': found,
                    'id': result['hits']['hits'][0]['_id']
                }
            else:
                return {
                    'found': found,
                    'id': None
                }
        except NotFoundError as e:
            found = False
            return {
                    'found': found,
                    'id': None
                }
    # used
    def search_audience_identity_by_email(self, email):
        found = False
        result = None

        query = {
            "query": {     
                "bool": {
                    "must": [
                        { "match": { 'business_email.keyword': email}},
                        { "match": { '_type': '@audience'}}
                    ]
                }
            }
        }
        try:
            result = self.es.search(body=json.dumps(query), index=self.index_name)
            found = (result['hits']['total']['value'] > 0)
            if found:
                return {
                    'found': found,
                    'id': result['hits']['hits'][0]['_id']
                }
            else:
                return {
                    'found': found,
                    'id': None
                }
        except NotFoundError as e:
            found = False
            return {
                    'found': found,
                    'id': None
                }

import os
from datetime import datetime

from elasticsearch import Elasticsearch, helpers
from elasticsearch.exceptions import NotFoundError


class AudienceProvider:

    # provider functions
    def __init__(self, index, user, pwd) -> None:
        self._index = index
        self._user = user
        self._pwd = pwd
        self._host = os.environ['HOST']
        self._port = int(os.environ["PORT"])
        self._use_ssl = bool(os.environ["USE_SSL"])
        self._elasticsearch = Elasticsearch(
            host=self._host, http_auth=(self._user, self._pwd), 
            port=self._port, use_ssl=self._use_ssl, verify_certs=False)

    @property
    def index_name(self):
        return self._index

    @property
    def es(self):
        return self._elasticsearch

    @property
    def max_workers(self):
        return 10


    # to be removed or udpated in future
    def update_new_properties(self, audience_profile):
        business_type = audience_profile["business_type"]
        job_function = audience_profile["job_function"]

        business_types = [{
            "id": business_type,
            "division": "Informa Markets",
            "brand": "Health and Nutrition"
        }]

        job_functions = [{
            "id": job_function,
            "division": "Informa Markets",
            "brand": "Health and Nutrition"
        }]
        audience_profile["business_types"] = business_types
        audience_profile["job_functions"] = job_functions

    # used
    def get_audience_profile_by_id(self, id):
        found = False
        result = None
        try:
            result = self.es.get(index=self.index_name, id=id)
            found = (result['found'])
            if found:
                return {
                    'found': found,
                    'data': [result]
                }
            else:
                return {
                    'found': found,
                    'data': None
                }
        except NotFoundError as e:
            found = False
            return {
                    'found': found,
                    'data': None
                }

    #used
    def add_audience_profile(self, audience_profile):
        if self.add_audience_profiles([audience_profile]):
            return audience_profile
    
    #used
    def add_audience_profiles(self, audience_profiles):
        for document in audience_profiles:
            document['metadata']['updated_on'] = datetime.now().strftime('%d-%b-%Y %H:%M:%S')

            # to be removed or updated in future
            self.update_new_properties(document)
    
        helpers.bulk(self.es, audience_profiles)
        return True

    # used
    def update_audience_profile(self, id, audience_profile_update): 

        saved_audience_profile = self.get_audience_profile_by_id(id)

        if (saved_audience_profile['found']):
            saved_document = saved_audience_profile['data'][0]
            saved_audience = saved_document['_source']
            updated_audience_profile = {
                '_index': saved_document['_index'], 
                '_type': saved_document['_type'], 
                '_id': saved_document['_id']
                }
            for key in saved_audience.keys(): 
                if key != 'social_accounts':     
                    if ((key in saved_audience) and (key in audience_profile_update)):
                        updated_audience_profile[key] = audience_profile_update[key]
                    if((key in saved_audience) and (key not in audience_profile_update)):
                        updated_audience_profile[key] = saved_audience[key]
                else:
                    if key in audience_profile_update:
                        updated_audience_profile[key] = saved_audience[key]
                        self.update_social_accounts(updated_audience_profile[key], audience_profile_update[key])

            # to be removed or updated in future
            self.update_new_properties(updated_audience_profile)
            self.add_audience_profile(updated_audience_profile)
            return updated_audience_profile
        else:
            return None

    def update_social_accounts(self, existing_accounts, accounts_update):
        if len(accounts_update) > 0:
            for account_update in accounts_update:
                account = self.get_social_account_by_type(existing_accounts, account_update['social_type'])
                if account != None:
                    account['social_email'] = account_update['social_email']
                else:
                    existing_accounts.append(account_update)

    def get_social_account_by_type(self, accounts, type):
        for account in accounts:
            if account['social_type'] == type:
                return account
        return None












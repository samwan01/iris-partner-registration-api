# Temporary fix for multi-route custom domain mapping issue

def create_api_mapping_v2(self, base_path_args):
    # type: (Dict[str, Any]) -> Dict[str, str]
    
    kwargs = {
        'ApiId': base_path_args['restApiId'],
        'ApiMappingKey':  base_path_args['basePath'],
        'DomainName': base_path_args['domainName'],
        'Stage': base_path_args['stage']            
    }

    result = self._client('apigatewayv2').create_api_mapping(
        **kwargs
    )
    if result['ApiMappingKey'] == '(none)':
        base_path = "/"
    else:
        base_path = "/%s" % result['ApiMappingKey']
    base_path_mapping = {
        'key': base_path
    }
    return base_path_mapping

def delete_api_mapping_v2(self, domain_name, path_key):
    # type: (str, str) -> None
    client = self._client('apigatewayv2')
    
    mappings = client.get_api_mappings(DomainName=domain_name)

    apiMappings = [x['ApiMappingId'] for x in mappings['Items'] if x['ApiMappingKey'] == path_key]

    if apiMappings:
        params = {
            'ApiMappingId': apiMappings[0],
            'DomainName': domain_name
        }
        client.delete_api_mapping(**params)
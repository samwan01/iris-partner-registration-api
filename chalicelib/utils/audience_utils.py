import base64
import os
import uuid
from datetime import datetime
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from re import sub

import boto3
import jwt
import segno
from botocore.exceptions import ClientError
from chalice import Response
from chalicelib.providers.audience import AudienceProvider
from chalicelib.providers.identity import IdentityProvider
from chalicelib.utils.file_utils import find_file
from pymongo import MongoClient


def get_index_name(name):
    return os.environ[name]

def get_es_user(app):
    if app.es_user == None:
        parameter = boto3.client('ssm').get_parameter(Name=os.environ['ES_USER'], WithDecryption=True)
        app.es_user = parameter['Parameter']['Value']
    return app.es_user

def get_es_pwd(app):
    if app.es_pwd == None:
        parameter = boto3.client('ssm').get_parameter(Name=os.environ['ES_PWD'], WithDecryption=True)
        app.es_pwd = parameter['Parameter']['Value']
    return app.es_pwd

def get_audience_provider(app):
    return AudienceProvider(get_index_name('AUDIENCE_INDEX_NAME'), get_es_user(app), get_es_pwd(app))    

def get_identity_provider(app):
    return IdentityProvider(get_index_name('IDENTITY_INDEX_NAME'), get_es_user(app), get_es_pwd(app))

def get_db_uri():
    parameter = boto3.client('ssm').get_parameter(Name=os.environ['DB_PWD'], WithDecryption=True)
    db_pwd = parameter['Parameter']['Value']
    uname_parameter = boto3.client('ssm').get_parameter(Name=os.environ['DB_USER'], WithDecryption=True)
    db_user = uname_parameter['Parameter']['Value']
    cert_file_path = find_file(os.environ["DB_CERT"])
    uri = "mongodb://" + db_user + ":" + db_pwd + "@" + os.environ["DB_SOCKET"] + "&tlsCAFile=" + cert_file_path
    return uri

def refactor_audience(audience):
    fields_to_remove = ['_index','_type','_id']
    for key in fields_to_remove:
        if key in audience:
            del audience[key]
    return audience

def prepare_audience(audience_identity_id, request):
    audience_profile = {}
    audience_profile['_index'] = get_index_name('AUDIENCE_INDEX_NAME')
    audience_profile['_type'] = '@audience'
    audience_profile['_id'] = audience_identity_id
    audience_profile['id'] = audience_identity_id

    for key in request:
        audience_profile[key] = request[key]

    if 'social_accounts' not in request:
        audience_profile['social_accounts'] = []

    if 'business_types' not in request:
        audience_profile['business_types'] = []

    if 'job_functions' not in request:
        audience_profile['job_functions'] = []

    audience_profile['metadata'] = {'updated_on': ''}
    
    return audience_profile

def prepare_identity(audience):
    identity = {
        'business_email': audience['business_email'],
        'first_name': audience['first_name'],
        'last_name': audience['last_name'],
        'social_accounts': audience['social_accounts'] if 'social_accounts' in audience else []
    }
    return identity

def save_audience_in_db(document, is_update):
    try:
        # connect to mongo
        client = MongoClient(get_db_uri(), retryWrites=False)
        audience = client[os.environ["DB_NAME"]][os.environ["AUDIENCE_COLLECTION"]]
        # remove unwanted fields from the document
        del document['_index']
        del document['_type']
        if is_update:
            audience.update_one({'_id':document['_id']}, {"$set": document}, upsert=True)
        else:
            audience.insert_one(document)
    except Exception as error:
        print(error)
        return False
    finally:
        client.close()
    return True

def get_success_response(data):
    response = {
        'status': 'success',
        'message': '',
        'data': [refactor_audience(data)]
    }
    return Response(body=response, headers={'Content-Type': 'application/json'}, status_code=200)

def get_pending_response(data):
    response = {
        'status': 'pending',
        'message': '',
        'data': [refactor_audience(data)]
    }
    return Response(body=response, headers={'Content-Type': 'application/json'}, status_code=200)

def get_error_response(message, error_code):
    response = {
        'status': 'error',
        'message': message,
        'data': []
    }
    return Response(body=response, headers={'Content-Type': 'application/json'}, status_code=error_code)

def refactor_audience_registration(audience_registration):
    fields_to_remove = ['_index','_type','_id', 'id']
    for key in fields_to_remove:
        if key in audience_registration:
            del audience_registration[key]
    return audience_registration

def register_audience_to_product(audience_profile, request):
    try:
        audience_profile_id = request["iris_id"]
        product_id = request["product_id"]
        # connect to mongo
        client = MongoClient(get_db_uri(), retryWrites=False)
        audience_registration_collection = client[os.environ["DB_NAME"]][os.environ["AUDIENCE_REGISTRATION_TO_PRODUCT"]]
        audience_registration_count = audience_registration_collection.count_documents({"iris_id": audience_profile_id, "product_id": product_id})
        if audience_registration_count == 0:
            registration_id = str(uuid.uuid4())
            request["_id"] = registration_id
            audience_registration_collection.insert_one(request)
            sendEmail(audience_profile, request)
        else:
            # return get_error_response('Audience Already Registered To Product.', 409)
            request = refactor_audience_registration(request)
            audience_registration_collection.update_one({"iris_id": audience_profile_id, "product_id": product_id}, {"$set": request}, upsert=True)
        
        return get_registered_audience_for_product(audience_profile_id, product_id)
    except Exception as error:
        print(error)
        return get_error_response('Something Went Wrong.', 500)
    finally:
        if client:
            client.close() 

def get_registered_audience_for_product(audience_profile_id, product_id):
    try:
        # connect to mongo
        client = MongoClient(get_db_uri(), retryWrites=False)
        audience_registration_collection = client[os.environ["DB_NAME"]][os.environ["AUDIENCE_REGISTRATION_TO_PRODUCT"]]
        audience_registration_cursor = audience_registration_collection.find({"iris_id":audience_profile_id, "product_id": product_id})
        registered_audience = []
        for document in audience_registration_cursor:
            registered_audience.append(refactor_audience_registration(document))

        if len(registered_audience) == 0:
            return get_error_response('Audience Not Registered For Product.', 404)

        return get_success_response(registered_audience[0])
    except Exception as error:
        return get_error_response('Something Went Wrong.', 500)
    finally:
        if client:
            client.close()

def sendEmail(audience_profile, request):

    sender = os.environ['SENDER_EMAIL_ACCOUNT']

    recipient = audience_profile["business_email"]

    ses_region = os.environ['SES_REGION']

    # subject = "Your registration for the Informa Capital Markets Day 2021" 

    email_template_id = "generic"

    if "questions" in request:
        if  "user_enroll_type" in request['questions']:
            email_template_id = request['questions']['user_enroll_type']

    # email_template_id = "generic" # remove this line after adding qr logic
    registration_email = get_registration_email(email_template_id)
    body_html = registration_email["body"]
    body_html = replace_placeholders(audience_profile, body_html)

    subject = registration_email["subject"] if registration_email["subject"] != None else "Registration successful"

    charset = "UTF-8"

    client = boto3.client('ses',region_name=ses_region)

    msg = MIMEMultipart('mixed')
    msg['Subject'] = subject 
    msg['From'] = sender 
    msg['To'] = recipient

    qr_image = "qrcode" + audience_profile["id"] + ".png"

    # keep the QR code in lambda's tmp folder and delete it once attached
    temporary_folder = "/" + "t" + "m" + "p"
    # add QR code attachment 
    if email_template_id == "onsite":
        os.chdir(temporary_folder)
        
        first_name = audience_profile["first_name"]
        business_email = audience_profile["business_email"]
        job_title = audience_profile["job_title"]
        iris_id = audience_profile["id"]

        payload = first_name + "|" + business_email + "|" + job_title + "|" + iris_id
        payload_bytes = payload.encode("ascii")
        base64_bytes = base64.b64encode(payload_bytes)
        base64_string = base64_bytes.decode("ascii")

        qrcode_data = "https://register.iiris.com/event_home.html?" + base64_string
        qrcode_data = "https://c.iris.informa.com/r/tp2?u=" + qrcode_data 
        tracking_details = "&e=se&se_ca=eventaccess&se_ac=onsite&uid=REPLACE_IRISID&se_va=REPLACE_TIMESTAMP&tv=no-js-0.1.0&aid=iiris.investor.app"
        tracking_details = replace_placeholders(audience_profile, tracking_details)
        qrcode_data = qrcode_data + tracking_details

        qrcode = segno.make(qrcode_data)
        qrcode.save(qr_image, scale=7)

        att = MIMEApplication(open(temporary_folder + "/" + qr_image, 'rb').read())
        att.add_header('Content-Disposition', 'attachment', filename=os.path.basename(temporary_folder + "/" + qr_image))
        att.add_header('Content-ID', '<qrcode>')
        msg.attach(att)

    msg_body = MIMEMultipart('alternative')

    htmlpart = MIMEText(body_html.encode(charset), 'html', charset)

    msg_body.attach(htmlpart)

    msg.attach(msg_body)

    try:
        response = client.send_raw_email(
            Source=sender,
            Destinations=[
                recipient
            ],
            RawMessage={
                'Data':msg.as_string(),
            }
        )
    except ClientError as e:
        print(e.response['Error']['Message'])
    else:
        print("Email Sent For Audience Registration For Product. %s" % response['MessageId'])
        temporary_folder = "/" + "t" + "m" + "p"
        qr_image = "qrcode" + audience_profile["id"] + ".png"
        if os.path.exists(temporary_folder + "/" + qr_image):
            os.remove(temporary_folder + "/" + qr_image)
            print("Removed the file /tmp/ %s" % qr_image)     
        else:
            print("Sorry, file does not exist. /tmp/ %s" % qr_image)

def get_registration_email(template_id): 
    try:
        # connect to mongo
        client = MongoClient(get_db_uri(), retryWrites=False)
        collection = client[os.environ["DB_NAME"]][os.environ["REGISTRATION_EMAIL_TEMPLATE"]]
        cursor = collection.find({"id":template_id})
        array = []
        for document in cursor:
            array.append(refactor_audience_registration(document))

        if len(array) == 0:
            print('get_registration_email - Email Template Not Found.')
            return ""

        return array[0]
    except Exception as error:
        print('get_registration_email - Something Went Wrong.')
    finally:
        if client:
            client.close()

def replace_placeholders(audience_profile, body):
    audience_profile_id = audience_profile["id"]
    first_name = audience_profile["first_name"]
    business_email = audience_profile["business_email"]
    job_title = audience_profile["job_title"]
    now = datetime.now()
    timestamp = datetime.timestamp(now)

    body = body.replace("REPLACE_IRISID", audience_profile_id)
    body = body.replace("REPLACE_FIRST_NAME", first_name)
    body = body.replace("REPLACE_BUSINESS_EMAIL", business_email)
    body = body.replace("REPLACE_JOB_TITLE", job_title)
    body = body.replace("REPLACE_TIMESTAMP", str(timestamp))
    return body

def get_email_from_request(app):
    email = None
    if 'authorization' in app.current_request.headers:
        token = app.current_request.headers['authorization']
        if token != None:
            token = token.replace('Bearer ', '').strip()
            decoded_token = jwt.decode(token, options={"verify_signature": False})
            if 'email' in decoded_token:
                email = decoded_token['email']
                return email
            else:
                return email
    else:
        return email

# This function checks if the user is trying to update his own record 
def user_authorized(request_email, audience):
    is_authorized = False

    if request_email == audience['business_email']:
        return True

    if 'social_accounts' in audience:
        social_accounts = audience['social_accounts']
        for account in social_accounts:
            social_email = account['social_email']
            if request_email == social_email:
                return True

    return is_authorized

from chalice import ForbiddenError
from fnmatch import fnmatch

def in_allowed_groups(request):

    if not 'authorizer' in request.context:
        return True

    groups = request.context['authorizer']['claims'].get('cognito:groups', '')

    allowed_resources = groups.split(',') if len(groups) > 1 else groups
    resource_to_check = request.method + remove_version_from_path(request.context['path'])

    for resource in allowed_resources:
        if fnmatch(resource_to_check, resource):
            return True

    return False

def remove_version_from_path(path):

    if path is not None:
        
        path_entries = path.lstrip('/').split('/')

        if len(path_entries) > 1:

            return '/' + '/'.join(path_entries[1:])

    return path
    